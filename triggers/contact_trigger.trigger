trigger contact_trigger on Contact (before insert,before update) {
  
    if(trigger.isinsert || trigger.isupdate){
        map<id,account> acmap= new map<id,account>([select id,email__c from account]);
        for(contact c:trigger.new){
            if(c.accountid !=null){
                c.Email=acmap.get(c.AccountId).email__c;
            }            
        }
    }
}