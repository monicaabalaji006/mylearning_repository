trigger pur_trigger on purchase__c (after insert,after update,after delete,after undelete) {
    set<id>  setid=new set<id>();
    if((trigger.isinsert || trigger.isupdate )&& trigger.isafter){
       
        for(purchase__c pur:trigger.new){
            setid.add(pur.customer__c);
        }
    }
        if((trigger.isdelete)&& trigger.isafter){
       
        for(purchase__c pur:trigger.old){
            setid.add(pur.customer__c);
        }
        }
 list<customer__c> licus=[select name,id,total_purchase__c,(select id,total__c from purchases__r) from customer__c where id in:setid];

        for(customer__c c:licus){
            decimal d=0;
            for(purchase__c p:c.purchases__r){
                d=d+p.total__c;
                }
            c.total_purchase__c=d;
            
        }
        update licus;
        system.debug('list'+licus);
            
        }