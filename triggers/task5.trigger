trigger task5 on purchase__c (after insert) {
    
    decimal c;
    set<Id> sid = new set<Id>();
    set<Id> cid = new set<Id>();
    
    if(trigger.isinsert && trigger.isafter){
        
        for (purchase__c p : Trigger.new) {
            
            sid.add(p.furniture1__c);
            c=p.Quantity__c;
        }
        
        list<furniture__c> fur=[select name,items_sold__c from furniture__c where id in:sid];
        for(furniture__c fc:fur)
        {
            fc.items_sold__c=fc.items_sold__c+c;
        }
        update fur;
        
        
        for (purchase__c pc : Trigger.new) {
            
            cid.add(pc.customer__c);
            
        }
        
        list<purchase__c> plist=[select name,furniture__r.furniture_type__c,furniture1__c,customer__c from purchase__c where customer__c in:cid];       
        map<id,integer> m=new map<id,integer>();
        
        integer count=0;
        for(purchase__c  p:plist)
        {
            if(m.containskey(p.furniture1__c))
            {
                m.put(p.furniture1__c,count+1);
            }
            else
            {
                m.put(p.furniture1__c,count);
            }
            
        }
        
        integer max=0; 
        id id1;
        for(Id i:m.keyset())
        {
            if(m.get(i)>max)
                max=m.get(i);
            id1=i;
            
        }
        
        list<Furniture__c> flist=new list<Furniture__c>([select name ,Furniture_Type__c from Furniture__c where id=:id1]);
        string ftype;
        
        for(Furniture__c f:flist)
        {
            ftype=f.Furniture_Type__c;
        }
        
        list< Customer__c> culist=new list<Customer__c>([select name ,prefer__c from Customer__c where id in: cid ]);    
        
        
        for(Customer__c c:culist)
        {
            c.prefer__c=ftype;
            
        }
        
        
        
        update culist;
        
    }
    
    
}