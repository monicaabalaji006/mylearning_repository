({
	doInit : function(component, event, helper) {
		
        var action = component.get("c.mycustomers");
        action.setParams({"FurnitureId":component.get("v.recordId")});
        
        action.setCallback(this,function(result)
                           {
                               var state = result.getState();
                               if (state =="SUCCESS")
                               {
                                   
                                                              
                                   component.set("v.customers",result.getReturnValue());
                            		
                               }
                           });
        $A.enqueueAction(action);
    },
    
    handleComponentEvent : function(component, event) {
        var message = event.getParam("message"); 
        alert(message);
        // set the handler attributes based on event data
       // component.set("v.messageFromEvent", message);
        
        var action = component.get("c.fetchDetails");
        action.setParams({
            CustomerId : message
        });
        action.setCallback(this,function(result)
                           {
                               var state = result.getState();
                               if (state =="SUCCESS")
                               {
                                   
                                  // var v=result.getReturnValue();                              
                                   component.set("v.CustomerDetail",result.getReturnValue());
                                                                           
                               		
                               }
                           });
        $A.enqueueAction(action);
     
    }
})