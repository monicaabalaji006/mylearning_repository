({
    
    doinit : function(cmp, event, helper) {
        var rid=cmp.get("v.recordId");
          var action1 = cmp.get("c.getrecord");
        action1.setParams({'fid':rid});
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
              //  alert('dfd'+response.getReturnValue());
                cmp.set("v.clist1", response.getReturnValue());
                
            }
            
        } );
       
        $A.enqueueAction(action1);
    },
    
	handleComponentEvent : function(cmp, event, helper) {
		 var message = event.getParam("message");
	//var action1=cmp.get("c.getrecord")
    
        // set the handler attributes based on event data
        cmp.set("v.messageFromEvent", message);

    }
})