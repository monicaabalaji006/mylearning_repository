public class query_update_save {
    
public opportunity newCon {get;set;}
      
public ApexPages.StandardSetController setCon {
        get {
            newcon= new opportunity();
            if(setCon == null) {
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator([select name,closedate,createddate from opportunity  order by createddate DESC]));
            }
            return setCon;
        }
        set;
    }
    
    public List<Opportunity> getOpportunities() {
         return (List<Opportunity>) setCon.getRecords();
         
    }
    
     public pagereference save(){
    
      insert newCon;

      Pagereference pr = New PageReference('/' + newCon.id);
      return pr;
   }
}