public with sharing class getCustomers {
	@AuraEnabled
    public static List<Purchase__c> mycustomers(String FurnitureId){
        system.debug(FurnitureId);
        List<Purchase__c> custList=[select Customer__r.Name from Purchase__c where Furniture__c=:FurnitureId];
        system.debug(custList);
        return custList;
    }
    
    @AuraEnabled
    public static List < Customer__c > fetchDetails(String CustomerId) {
        
        List < Customer__c > returnList = new List < Customer__c > ();
        
        for (Customer__c cus: [select Name, Phone__c, Email__c, Total_Purchase__c from Customer__c where id=:CustomerId]) {
            returnList.add(cus);
        }
        return returnList;
    }
}