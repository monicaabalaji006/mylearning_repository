public class ExtendCustomers {
    
	public Contact newCon {get;set;}
    public account newacc {get;set;}

   public ExtendCustomers(ApexPages.StandardController con){
      newCon = (Contact)con.getRecord();
      newacc= new account();
   }

   public pagereference save(){
      
      Insert newacc;
      newCon.AccountID = newacc.id;
      insert newCon;

      Pagereference pr = New PageReference('/' + newacc.id);
      return pr;
   }

}