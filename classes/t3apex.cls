public class t3apex {
    @AuraEnabled
    public static list<customer__c> getrecord(string fid){
        
        set<id> pid=new set<id>();
        list<purchase__c> p=[select id,name,customer__c,customer__r.name,furniture__c from purchase__c where furniture__c =: fid];
        for(purchase__c pur:p)
        {
            pid.add(pur.customer__c);
        }
          
        list<customer__c> c = [select Name,Phone__c,email__c from customer__c where id=:pid];
        return c;
        
    }
}